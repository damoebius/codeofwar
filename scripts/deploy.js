console.log('Deploy Prod');
var process = require("process");
var deploy = require("happy-deploy");
var PackageTask = deploy.PackageTask;
var SendSSHTask = deploy.SendSSHTask;
var ExecSSHTask = deploy.ExecSSHTask;

deploy.addTask(new PackageTask(__dirname+'/../build', __dirname+'/../codeofwar.tar.gz'));

var password = process.argv[2];

deploy.addTask(new SendSSHTask(
    'qualif.codeofwar.net',
    22,
    'tamina',
    password,
    __dirname+'/../codeofwar.tar.gz',
    '/home/tamina/projects/codeofwar.tar.gz',
    '/home/tamina/projects/codeofwar-build'
));


deploy.addTask(new ExecSSHTask(
    'qualif.codeofwar.net',
    22,
    'tamina',
    password,
    null,
    'cd /home/tamina/projects && ./update-codeofwar.sh'
));

deploy.run();
